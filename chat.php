<?php
	require_once("action/ChatAction.php");

	$action = new ChatAction();
	$action->execute();

	require_once("partial/header.php");
?>

<script type="text/javascript" src="js/chat.js"></script>

<?php
	$theme = $action->theme;

	if($theme === "pow" || $theme === "fzero" || $theme === "sax"){
?>
		<script type="text/javascript" src="js/<?=$theme?>.js"></script>
<?php
	} else {
?>
		<script type='text/javascript'>

			alert("Ce thème n'existe pas!");

		</script>
<?php
	}
?>

<div class="container" id="chat">
	<div class="row">
		<div class="col-sm-9" id="msgBox">
			<!-- fill this with messages -->
			<ul class="list-group" id="msgList">
			</ul>
		</div>

		<div class="col-sm-3" id="usrBox">
			<!-- fill this with users -->
			<ul class="list-group" id="usrList">
			</ul>

		</div>
	</div>

	<div class="row">
		<div class="col-sm-9" id="txtBox">
    		<input type="text" class="form-control" id="inputText" placeholder="Say something, genius!">		
		</div>

		<div class="col-sm-3 text-center" id="btnBox">
			<button class="btn btn-default" name="send" id="send">Send</button>
			<button class="btn btn-default col-sm-offset-2" name="logout" id="logout">Logout</button>
		</div>
	</div>
</div>



<?php 
	require_once("partial/footer.php");