var particles = []
var bottomBorder = window.innerHeight - 50
var requestId = null
var maxLiNb = 50 //maximum number of li to take into consideration to reduce lag if there are lots of messages
var usrListOffset = 70

setup()

$(function(){
	$("body").append('<div class="row" id="powWrap"><div class="center-block text-center"><img src="images/pow.png" id="pow" class="btn"></div></div>')

	$("#pow").click(function(){
		animatePow()		
	})

	//adding background images to messages and users, without modifying the original javascript (chat.js)
	//Check every node that is being added. If it's an "li" and not a "particle" (because they would already have the classes for background image)
	//Apply new css class that has background-image, depending on whether its a message or a user
	$(document).bind('DOMNodeInserted', function(e) {
		var elem = $(e.target)
		if(elem.is("li")){
			if(!elem.hasClass("particle")){
				if(elem.hasClass("msg")){
					addBgImageMsg(elem)
				}
				else if(elem.hasClass("usr")){
					elem.addClass("usr-bg")
				}
			}
		}
	})

	tick()
})

function startTumble(){
	particleID = 0
	particles = []

	count = $("li").length;

	if(count <= maxLiNb){
		var i = 0
		$("li").each(function(index){
			$(this).addClass("particle")
			var offset = $(this).offset()
			var left = offset.left + usrListOffset
			var top = offset.top + offset.top * i //offset.top toujours égal a 35...bizarre! j'imagine que c'est la valeur pour le ul qui le contient?

			var txt = $(this).text()
			var id = "particle_" + particleID

			$(this).insertBefore("#chat") //faut déplacer les li en dehors du chat pour qu'ils traversent la page au complet et pas juste la boite du chat

			particles.push(new Particle($(this), left, top))

			particleID++
			i++
		})
	}
}

function addBgImageMsg(elem){
	var rand = Math.floor((Math.random() * 9)); //random integer from [0,9)

	elem.addClass("msg-bg")

	if(rand == 0){
		elem.addClass("turtle-green")
	}
	else if(rand == 1){
		elem.addClass("turtle-red")
	}
	else if(rand == 2){
		elem.addClass("turtle-purple")		
	}
	else if(rand == 3){
		elem.addClass("crab-green")		
	}
	else if(rand == 4){
		elem.addClass("crab-red")		
	}
	else if(rand == 5){
		elem.addClass("crab-blue")		
	}
	else if(rand == 6){
		elem.addClass("coin")		
	}
	else if(rand == 7){
		elem.addClass("fighterfly")		
	}
	else if(rand == 8){
		elem.addClass("slipice")		
	}
}

function animatePow(){
	//Start moving the block up
	//once that is done, start moving the chat up
	//once chat is done moving up, start the tumble
	//move chat back to origin
	//move block back to origin

	$("#pow").transition({
		translate:[0,-35]
	},60,function(){

			$("#chat").transition({ 	//transitionception
				translate:[0,-25]
			},50,function(){
				startTumble()
				$("#chat").transition({
					translate:[0,0]
				},50)
			})

		$("#pow").transition({
			translate:[0,0]
		},50)
	})
}

//Add everything needed for the script...css, additionnal js files
function setup(){
	$("head").append('<script type="text/javascript" src="js/sprite/Particle.js"></script>')
	$("head").append('<link rel="stylesheet" href="css/pow.css" type="text/css" />')
	$("head").append('<script type="text/javascript" src="js/jquery.transit.js"></script>')
}

function tick (){
	for (var i = 0; i < particles.length; i++) {
		particles[i].tick()

		if(particles[i].done){
			particles[i].elem.remove()
			particles.splice(i, 1)
			i--
		}
	}

	if(particles.length == 0){
		window.cancelAnimationFrame(requestId);
		animating = false
	}
	else{
		animating = true
	}

	requestId = requestAnimationFrame(tick);
}