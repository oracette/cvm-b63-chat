setup()

keys = ["E", "Y", "U", "I", "O", "P", "A", "S", "D", "F", "G", "H", "J", "K", "K", "L", "Z", "X", "C", "V", "B", "N", "N", "M"]
playNote = new Howl({})
playSong = new Howl({})
playingSong = false
noteID = 0
notes = []

$(function(){	
	sharp = false
	odds = 0.5

	esgKeyWords = ["epic sax guy", "sax loop", "asdf"]
	cwKeyWords = ["careless whisper","george michael","sexy sax man", "asdf"]


	saxPlayer = getSaxPlayer()

	$("#inputText").keydown(function(event){
		keyNum = event.which
		keyString = String.fromCharCode(event.which)

		if(keyNum === 16){
			sharp = true
		} else if(keyNum === 13){
			checkInput()
		}

		if($.inArray(keyString, keys) != -1){
			playSample(keyString, sharp)
			addNote()

			if(!playingSong){
				animatePlayer(saxPlayer)
			}
		}
	})

	$("#inputText").keyup(function(event){
		if(event.which === 16){
			sharp = false
		}
	})

	$("#send").click(function(event){
		checkInput()
		console.log("CLICK")
	})

	$("body").click(function (e){
        var posX = $(this).offset().left,
            posY = $(this).offset().top;

        console.log((e.pageX - posX) + ' , ' + (e.pageY - posY));
    })

    tick()
})

function addNote(){
	rand = Math.random()
	if(rand > odds){
		type = "noire"
	}
	else{
		type = "croche"
	}

	id = "note_" + noteID

	createStr = "<img src='images/" + type + ".png' class='note' id='" + id +"'>"

	$("body").append(createStr)

	left = saxPlayer.offset().left

	if(saxPlayer.attr('id') === "SSM"){
		player = 1
		left += 200
	}else{
		player = -1		
	}

	notes.push(new Note($("#" + id), left, 230, player))

	noteID++
}

function tick(){
	for (var i = 0; i < notes.length; i++) {
		if(notes[i].inWindow){
			notes[i].tick()
		}
		else{
			$("#" + notes[i].element.attr("id")).remove()
			notes.splice(i,1)
			i--
		}
	};

	window.requestAnimationFrame(tick);
}

function checkInput(){
	input = ($("#inputText").val()).toLowerCase()

	if(input === "!stop"){
		playSong.stop()
		playingSong = false
		if(typeof intervalId != 'undefined'){
			clearInterval(intervalId)
		}
	}

	if(saxPlayer.attr('id') === "ESG"){
		if(checkForKeyWords(input, esgKeyWords)){
			playSong = esgLoop
		 	playSong.play()
		 	animateDanceESG(saxPlayer)
		 	intervalId = setInterval(function(){
		 		animateDanceESG(saxPlayer)
		 	},460)
		 	playingSong = true
		}
	}
	else if(checkForKeyWords(input, cwKeyWords)){
			playSong = carelessWhisper
		 	playSong.play()
		 	setTimeout(function(){
		 		animateDanceSSM(saxPlayer)
		 		intervalId = setInterval(function(){
			 		animateDanceSSM(saxPlayer)
			 	},1620)
		 	}, 2000)
		 	playingSong = true
	}
}

function setup(){
	$("head").append('<script type="text/javascript" src="js/howler.js"></script>')
	$("head").append('<script type="text/javascript" src="js/jquery.transit.js"></script>')
	$("head").append('<script type="text/javascript" src="js/sprite/Note.js"></script>')
	$("head").append('<link rel="stylesheet" href="css/sax.css" type="text/css" />')
	loadSamples()
	loadClips()
}

function animatePlayer(player){	
	player.transition({
		scale:1.1
	}, 40, function(){
		player.transition({
			scale:1
		},40)
	})
}

function animateDanceSSM(player){
	player.transition({
		translate:[+30,0]
	},750,function(){
		player.transition({
			translate:[0,0]
		},750)
	})
}

function animateDanceESG(player){
	player.transition({
		translate:[-30,0]
	},30,function(){
		player.transition({
			translate:[0,0]
		},60)
	})
}

function checkForKeyWords(string, keywords){
	check = false

	if(string != "" && string != " "){
		for (var i = 0; i <  keywords.length; i++) {
			if(string.indexOf(keywords[i]) != -1)
				check = true
		}
	}

	return check
}

function getSaxPlayer(){
	rand = Math.random()
	player = null

	if(rand > odds){
		$("body").addClass("SSMbg")
		$("body").append("<img src='images/SSM.png' class='saxPlayer' id='SSM'>")
		player = $("#SSM")
	}
	else{
		$("body").addClass("ESGbg")
		$("body").append("<img src='images/ESG.png' class='saxPlayer' id='ESG'>")
		player = $("#ESG")
	}

	return player
}

function loadClips(){
	esgLoop = new Howl({
		urls:['audio/clips/Epic Sax Guy.ogg'],
		loop:true
	})

	carelessWhisper = new Howl({
		urls:['audio/clips/Careless Whisper.mp3'],
		loop:true
	})
}

function loadSamples(){ //most beautiful code I've ever written please refactor ;_;
	a1 = new Howl({
		urls:['audio/sax/1/a.mp3']
	})

	as1 = new Howl({
		urls:['audio/sax/1/as.mp3']
	})

	b1 = new Howl({
		urls:['audio/sax/1/b.mp3']
	})

	ds1 = new Howl({
		urls:['audio/sax/1/ds.mp3']
	})

	e1 = new Howl({
		urls:['audio/sax/1/e.mp3']
	})

	f1 = new Howl({
		urls:['audio/sax/1/f.mp3']
	})

	fs1 = new Howl({
		urls:['audio/sax/1/fs.mp3']
	})

	g1= new Howl({
		urls:['audio/sax/1/g.mp3']
	})

	gs1 = new Howl({
		urls:['audio/sax/1/gs.mp3']
	})



	a2 = new Howl({
		urls:['audio/sax/2/a.mp3']
	})

	as2 = new Howl({
		urls:['audio/sax/2/as.mp3']
	})

	b2 = new Howl({
		urls:['audio/sax/2/b.mp3']
	})

	c2 = new Howl({
		urls:['audio/sax/2/c.mp3']
	})

	cs2 = new Howl({
		urls:['audio/sax/2/cs.mp3']
	})

	d2 = new Howl({
		urls:['audio/sax/2/d.mp3']
	})

	ds2 = new Howl({
		urls:['audio/sax/2/ds.mp3']
	})

	e2 = new Howl({
		urls:['audio/sax/2/e.mp3']
	})

	f2 = new Howl({
		urls:['audio/sax/2/f.mp3']
	})

	fs2 = new Howl({
		urls:['audio/sax/2/fs.mp3']
	})

	g2 = new Howl({
		urls:['audio/sax/2/g.mp3']
	})

	gs2 = new Howl({
		urls:['audio/sax/2/gs.mp3']
	})




	a3 = new Howl({
		urls:['audio/sax/3/a.mp3']
	})

	as3 = new Howl({
		urls:['audio/sax/3/as.mp3']
	})

	b3 = new Howl({
		urls:['audio/sax/3/b.mp3']
	})

	c3 = new Howl({
		urls:['audio/sax/3/c.mp3']
	})

	cs3 = new Howl({
		urls:['audio/sax/3/cs.mp3']
	})

	d3 = new Howl({
		urls:['audio/sax/3/d.mp3']
	})

	ds3 = new Howl({
		urls:['audio/sax/3/ds.mp3']
	})

	e3 = new Howl({
		urls:['audio/sax/3/e.mp3']
	})

	f3 = new Howl({
		urls:['audio/sax/3/f.mp3']
	})

	fs3 = new Howl({
		urls:['audio/sax/3/fs.mp3']
	})

	g3 = new Howl({
		urls:['audio/sax/3/g.mp3']
	})

	gs3 = new Howl({
		urls:['audio/sax/3/gs.mp3']
	})



	c4 = new Howl({
		urls:['audio/sax/4/c.mp3']
	})

	cs4 = new Howl({
		urls:['audio/sax/4/cs.mp3']
	})

	d4 = new Howl({
		urls:['audio/sax/4/d.mp3']
	})
}
 
function playSample(key, sharp){ //most beautiful code I've ever written please refactor ;_;
	playNote.stop()

	if(key === "Y"){
		playNote = ds1
	}

	else if(key === "U"){
		playNote = e1
	}

	else if(key === "I"){
		if(sharp){
			playNote = fs1
		}
		else{
			playNote = f1
		}
	}

	else if(key === "O"){
		if(sharp){
			playNote = gs1
		}
		else{
			playNote = g1
		}
	}


	else if(key === "P"){
		if(sharp){
			playNote = as1
		}
		else{
			playNote = a1
		}
	}


	else if(key === "A"){
		playNote = b1
	}


	else if(key === "S"){
		if(sharp){
			playNote = cs2
		}
		else{
			playNote = c2
		}
	}

	else if(key === "D"){
		if(sharp){
			playNote = ds2
		}
		else{
			playNote = d2
		}
	}

	else if(key === "F"){
		playNote = e2
	}

	else if(key === "G"){
		if(sharp){
			playNote = fs2
		}
		else{
			playNote = f2
		}
	}

	else if(key === "H"){
		if(sharp){
			playNote = gs2
		}
		else{
			playNote = g2
		}
	}

	else if(key === "J"){
		if(sharp){
			playNote = as2
		}
		else{
			playNote = a2
		}
	}

	else if(key === "K"){
		playNote = b2
	}

	else if(key === "L"){
		if(sharp){
			playNote = cs3
		}
		else{
			playNote = c3
		}
	}

	else if(key === "Z"){
		if(sharp){
			playNote = ds3
		}
		else{
			playNote = d3
		}
	}

	else if(key === "X"){
		playNote = e3
	}	

	else if(key === "C"){
		if(sharp){
			playNote = fs3
		}
		else{
			playNote = f3
		}
	}

	else if(key === "V"){
		if(sharp){
			playNote = gs3
		}
		else{
			playNote = g3
		}
	}

	else if(key === "B"){
		if(sharp){
			playNote = as3
		}
		else{
			playNote = a3
		}
	}

	else if(key === "N"){
		playNote = b3
	}

	else if(key === "M"){
		if(sharp){
			playNote = cs4
		}
		else{
			playNote = c4
		}
	}

	else if(key === "E"){
		playNote = d4
	}

	playNote.play()
}