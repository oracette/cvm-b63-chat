$(function(){
	activeColor = "#8AE6B8"
	inactiveColor = "#7D7D80"
	activeBtn = null

	activeBtn = activateBtn($("#pow"))

	$('[data-toggle="tooltip"]').tooltip(); 

	$("#fzero").click(function(){
		activeBtn = activateBtn($("#fzero"))
	})

	$("#pow").click(function(){
		activeBtn = activateBtn($("#pow"))
	})

	$("#sax").click(function(){
		activeBtn = activateBtn($("#sax"))
	})

	$("#createAcc").click(function(){
		$("#createAccWrap").css("display", "block")
		$("#loginWrap").css("display", "none")
	})

	$("#hide").click(function(){
		$("#createAccWrap").css("display", "none")
		$("#loginWrap").css("display", "block")
	})
})

function showCreateAcc(){
	$("#createAccWrap").css("display", "block")
}

function activateBtn(button){
	if(activeBtn != null){
		if(activeBtn.attr('id') != button.attr('id')){
			activeBtn.css("border-color", inactiveColor)
			//change value of the hidden input field
			activeInput = "#" + activeBtn.attr('id') + "Input"
			$(activeInput).val("false")
		}		
	}

	button.css("border-color", activeColor)
	buttonInput = "#" + button.attr('id') + "Input"
	$(buttonInput).val("true")

	return button
}