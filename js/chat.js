$(function () {
	var cookies = document.cookie;
	var username = getCookie("user")

	setInterval(updateChat, 3000);
	setInterval(updateUsers, 5000);
	setInterval(function(){
		$("#msgBox").scrollTop($("#msgBox")[0].scrollHeight)
	}, 500)

	$("#logout").click(function() {
		logout()
	})

	$("#send").click(function() {
		sendMessage()
	})

	$('#txtBox').keyup(function (event) {
	    var keypressed = event.which;

	    if (keypressed == 13) {
	        sendMessage(username)
	    }
	});
})

//code pris de w3schools
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');

    for(var i=0; i<ca.length; i++) {
        var c = ca[i];

        while (c.charAt(0)==' ') {
        	c = c.substring(1);
        }

        if (c.indexOf(name) == 0){
        	return c.substring(name.length,c.length);
        } 
    }

    return "";
}

function updateUsers() {
	var updateUsers = true

	$.ajax({
		type: 'POST',
		url: 'ajax.php',
		data:{
			updateUsers: updateUsers
		}
	}).done(function(data){
		data = JSON.parse(data);
		if(data != null){
			$("#usrList").empty();
			
			for(i=0; i<data.length; i++){
				usr = data[i]				
				li = createListElement(usr)
				li.addClass("usr")
				$("#usrList").append(li)
			}
		}
	})
}

function updateChat() {
	var updateChat = true

	$.ajax({
		type: 'POST',
		url: 'ajax.php',
		data:{
			updateChat: updateChat
		}
	}).done(function(data){
		data = JSON.parse(data);

		for(i=0; i<data.length; i++){
			msg = data[i].nomUsager + ": " + data[i].message
			
			li = createListElement(msg)
			li.addClass("msg")
			$("#msgList").append(li)
		}
	})
}

//Je dois avouer que Gabriel (Beauchamp) m'a aidé avec ceci.
function createListElement(string){
	li = jQuery('<li>')
	li.attr('class','list-group-item')
	li.text(string)

	return li
}

function logout() {
	var logout = true

	$.ajax({
		type: 'POST',
		url: 'ajax.php',
		data:{
			logout: logout
		}
	}).done(function(){
		window.location.replace("index.php");
	})
}

function sendMessage(username){
	msg = $("#inputText").val()

	if(msg !== ""){
		selfMsg = username + ": " +  msg
		li = createListElement(selfMsg)
		li.addClass("msg")
		$("#msgList").append(li)		
		$("#inputText").val('')

		$.ajax({
			type: 'POST',
			url: 'ajax.php',
			data:{
				toSend: msg
			}
		})
	}
}