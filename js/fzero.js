//Don't stop or you will notice the clipping issues!!

//var for what to divide z index by for scaling
var scaling = 10

//var for z scaling speed
var zScaleSpeed = 0.05

//scaling multiplicator
var trackScaleMulti = 1/575

// Setup car and track
var car

var trackSections = []

var SEGM_ID = 0

//Setup controls
var controls = {
    forward: 38, 
    left:37, 
    right:39,
    chat:27
}

setup()

window.onload = init

function init(){

    //need to add these once the page is loaded for the slide, can't put it in setup()
    $('<div id="canvasWrap"></div>').insertAfter($("#chat"))
    $("#canvasWrap").append('<canvas id="canvas" width="800" height="600" tabindex="1"></canvas>')

    positionChat()

    //Press ESC to toggle chat
    $("body").keydown(function (event){
        key = event.which

        if(key == controls.chat){
            positionChat() //recall position chat, in case window was resized
            $("#chat").slideToggle("slow")
        } 
    })


    //canvas focused with tabindex...needs to be clicked on but it works!
    //Other method implies making your own focus. Some times its just better to cheat.
    $("canvas").keydown(function (event){
        key = event.which

        if (key == controls.forward){
            car.goingForward = true
        } 
        else if (key == controls.left){
            car.goingLeft = true
            car.rotation = car.rotationSpeed

        } 
        else if (key == controls.right){
            car.goingRight = true
            car.rotation = -car.rotationSpeed
        }
    })

    $("canvas").keyup(function (event){
        key = event.which

        if (key == controls.forward){
            car.goingForward = false
        } 
        else if (key == controls.left){
            car.goingLeft = false
            car.rotation = 0
        } 
        else if (key == controls.right){
            car.goingRight = false
            car.rotation = 0
        } 
    })

    $("canvas").click(function (e){
        var posX = $(this).offset().left,
            posY = $(this).offset().top;

        console.log((e.pageX - posX) + ' , ' + (e.pageY - posY));
    })

    $("#slider").click(function (e){
        $("#chat").slideToggle("slow")
    })

    car = new BlueFalcon()

    //could make an entire map with this
    trackSections.push(new TrackSection(0,0,400,0,575))

    tick()
}

function positionChat(){
    canvasPos = $("canvas").offset()
    $("#chat").css(canvasPos)
}

//setup needed libraries, css, class files...
function setup(){
    $("head").append('<script type="text/javascript" src="js/sprite/BlueFalcon.js"></script>')
    $("head").append('<script type="text/javascript" src="js/sprite/Orb.js"></script>')
    $("head").append('<script type="text/javascript" src="js/sprite/TrackSegment.js"></script>')
    $("head").append('<script type="text/javascript" src="js/sprite/TrackSection.js"></script>')
    $("head").append('<script type="text/javascript" src="js/jcanvas.js"></script>')
    $("head").append('<link rel="stylesheet" href="css/fzero.css" type="text/css" />')
}

function tick(){
     $('canvas').clearCanvas()

    tsToCheck = [] //contains segments to check for collisions

    renderList = {} //contains ids and segments of segments to render

   //Pour chaque segment dans une section
   //affiche s'il est dans le canvas --> ajoute le à la liste de choses a afficher
   //s'il est entre les balises de collision (z de 400 à 600) check s'il y a des collisions pour chaque orb
   //s'il est le premier de sa section et qu'il n'est plus affiché, générer une nouvelle section
   //s'il est le dernier de sa section et qu'il n'est plus affiché, delete la section


    currentSection = car.currentSection
    nextSection = currentSection + 1

    for (var i = 0; i < trackSections.length; i++) {
        for (var j = 0; j < trackSections[i].segments.length; j++) {
            trackSections[i].segments[j].tick(car.speed)
            trackSections[i].segments[j].orbs[0].tick(car.speed, trackSections[i].segments[j], true)
            trackSections[i].segments[j].orbs[1].tick(car.speed, trackSections[i].segments[j], false)

            if(trackSections[i].segments[j].inCanvas){
                renderList[trackSections[i].segments[j].id] = trackSections[i].segments[j]
            }

            if(trackSections[i].segments[j].z >= 400 && trackSections[i].segments[j].z <= 600){
                tsToCheck.push(trackSections[i].segments[j])
            }
        }

        if(trackSections[i].segments[0].passed && trackSections[i + 1] == undefined){
            lastX = trackSections[i].segments[trackSections[i].segments.length - 1].x
            lastZ = trackSections[i].segments[trackSections[i].segments.length - 1].z -5

            addTrackSection(lastX, lastZ)
        }

        if(trackSections[i].segments[trackSections[i].segments.length - 1].passed){
            trackSections.splice(i,1)
            i--
        }
    }

    for (var id in renderList) {
        drawTrackSegment(renderList[id])
    }
    
    for(var i = 0; i < tsToCheck.length; i++){
        car.checkCollision(tsToCheck[i])
    }

    car.tick()

    drawCar()

    window.requestAnimationFrame(tick)
}

function drawCar(){
     $('canvas').drawImage({
        source: car.spriteSheet,
        x: car.x,
        y: car.y,
        sWidth: car.spriteWidth,
        sHeight: car.spriteHeight,
        sx: car.spriteRotate * car.spriteWidth,
        sy: 0,
        scale: car.scale - car.z/scaling
    })   
}

function drawTrackSegment(ts){
     $('canvas').drawImage({
        source: ts.spriteSheet,
        x: ts.x,
        y: ts.z,
        scale: ts.scale
    }) 
}

function calcScale(z){
    //le scale doit incrémenter d'apres le z, selon une fonction linéaire
    //y = ax + b
    //ou y = scale
    //  x = z
    // a = multiplicateur quelquonque
    // b = ??? --> pas besoin

    //Un peu d'algebre...  
    // y - b = ax
    // (y-b) / x = a
    //scale / z = multiplicateur

    //On sait que pour un z de 575 le scale est de 1.
    //1/575 = multiplicateur


    return z * trackScaleMulti
}

function addTrackSection(lastX, lastZ){
    typeRand = Math.random()
    turnIntensity = Math.random() * 20

    if(typeRand < 0.5){
        type = 0
        turnIntensity = 0
    }
    else if(typeRand >= 0.5 && typeRand < 0.74){
        type = 1
    }
    else if(typeRand >= 0.74 && typeRand < 1){
        type = 2
        
    }

    //Silly patch
    //Pour ne pas générer de section qui tournent en dehors du canvas
    if(lastX < 400){
        type = 2
    }
    else if(lastX > 400){
        type = 1
    }

    trackSections.push(new TrackSection(type,turnIntensity,lastX,0,lastZ))
}

function addNewSegment(prevSegment){
    x = 400
    y = 0
    z = prevSegment.z - prevSegment.spriteHeight + 2

    scale = calcScale(z)

    if(prevSegment.type == 2){
        trackList.push(new TrackSegment(x,y,z, scale, "images/track_seg_1.png",1))
    }
    else{
        trackList.push(new TrackSegment(x,y,z, scale, "images/track_seg_2.png",2))
    }
}

function removeSegment(ts){
    $('canvas').clearCanvas({
        x:ts.x, 
        y:ts.z,
        width: ts.spriteWidth,
        height: ts.spriteHeight
    })
}