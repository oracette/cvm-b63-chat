function Orb(x,y,z, scale){
	this.x = x
	this.y = y
	this.z = z
	this.radius = 50
	this.scale = scale

	// For debug purposes
	// this.spriteSheet = new Image()
	// this.spriteSheet.src = "images/mite.jpg"
}

Orb.prototype.tick = function(carSpeed,ts, left){
	if(carSpeed > 0){
		if(left){
			this.x = ts.x - ts.spriteWidth/2 - 30

			this.z = ts.z + ts.spriteHeight/2 + 20
		}
		else{
			this.x = ts.x + ts.spriteWidth/2 - 10

			this.z = ts.z - ts.spriteHeight/2 + 20
		}

		this.y = ts.y

		this.scale = ts.scale
		this.radius = 50 * scale
	}
}