function TrackSegment(id, x, y, z, scale,type) { 
	this.spriteWidth = 900 * scale
	this.spriteHeight = 50 * scale
	this.spriteSheet = new Image()
	this.spriteSheet.src = "images/track_seg_" + type + ".png"
	this.x = x
	this.y = y
	this.z = z
	this.scale = scale
	this.inCanvas = true
	this.type = type
	this.rotation = 0
	this.orbs = [new Orb(this.x - this.spriteWidth/2 - 10,this.y,this.z + this.spriteHeight/2 + 20, this.scale), new Orb(this.x + this.spriteWidth/2 - 20,this.y,this.z - this.spriteHeight/2 + 20, this.scale)]
	this.id = id
	this.passed = false
}

TrackSegment.prototype.tick = function(carSpeed){
	this.z *= (1 + carSpeed/100)

	this.scale = calcScale(this.z)

	this.spriteWidth = 900 * this.scale
	this.spriteHeight = 50 * this.scale

	if(this.z >= 60 && this.z < 630){
		this.inCanvas = true
	}
	else{
		this.inCanvas = false
	}

	if(this.z > 630){
		this.passed = true
	}
}