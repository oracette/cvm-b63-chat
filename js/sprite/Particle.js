function Particle(elem, x, y){
	this.x = x
	this.y = y
	this.ySpeed = Math.random() * 10
	this.yVelocity = Math.random() * 1 + 0.2 //max 1, minimum 0.2
	this.elem = elem
	this.done = false
	this.deg = 0
	this.rotateySpeed = Math.random() * 5 + 1
	this.minySpeed = 0.1
	this.oldY = null
	this.rotateStopOffset = 20
	this.dir = this.getDir()
	this.xSpeed = Math.random() * 10 * this.dir //dir is either -1 or 1, meaning left or right.
	this.xVelocity = Math.random() * 1 + 0.2
}

Particle.prototype.tick = function(){
	this.oldY = this.y

	this.ySpeed += this.yVelocity
	this.xVelocity += this.xVelocity

	this.y += this.ySpeed
	this.x += this.xSpeed

	if(this.y > bottomBorder){
		this.y = bottomBorder
		this.ySpeed = -this.ySpeed/1.5

		if(this.dir == 1){
			if(this.xSpeed > 0){
				this.xSpeed -= 1

				if(this.xSpeed < 0){
					this.xSpeed = 0
				}
			}
		}
		else{
			if(this.xSpeed < 0){				
				this.xSpeed += 1

				if(this.xSpeed > 0){
					this.xSpeed = 0
				}
			}
		}
	}
	
	if(this.y < bottomBorder - this.rotateStopOffset){
		this.deg = this.rotate(this.deg)
	}
	
	if (this.oldY == this.y) {
		this.ySpeed = 0;
		this.yVelocityY = 0;
		this.done = true
	}

	this.elem.offset({
		top:this.y,
		left:this.x
	})
}

Particle.prototype.rotate = function(deg){
	this.elem.css("transform", "rotate("+ deg + "deg)")

	return deg + this.rotateySpeed
}

Particle.prototype.getDir = function(){
	var rand = Math.random()
	var dir = 0

	if(rand < 0.5){
		dir = 1
	}
	else{
		dir = -1
	}

	return dir
}