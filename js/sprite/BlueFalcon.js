function BlueFalcon() { 
	this.spriteWidth = 70
	this.spriteHeight = 55
	this.spriteSheet = new Image()
	this.spriteSheet.src = "images/blue_falcon.png"
	this.x = ($('canvas').width() - this.spriteWidth/2) / 2 + 18
	this.y = $('canvas').height() - 100
	this.z = 0
	this.speed = 0	//arbitrary value atm. speed of car.
	this.maxSpeed = 6
	this.velocity = 0.1
	this.slowDown = 0.15
	this.turnSpeed = 6
	this.maxTurnSpeed = 7
	this.friction = 0.5 //arbitrary value atm. friction when turning.
	this.rotation = 0
	this.rotationSpeed = 10
	this.spriteRotate = 5 //index of the sprite representing rotation of the car, starting from 1. 0 = going right, 5 = Going forward, 10 = going left...lol reversed.
	this.scale = 1
	this.goingForward = false
	this.goingLeft = false
	this.goingRight = false
	this.fixed = true
	this.collCircleRad = this.spriteWidth/2 - 5 //circle for collision
	this.colliding = false
	//Collision Resolve Timer
	this.crt = 0
	this.maxCRT = 4
	//Collision Resolve Slowdown
	this.crs = 10
	this.leftCollide = false
	this.rightCollide = true
	this.collOrb = null
	this.collDist = 0
	this.currentSection = 0
}

BlueFalcon.prototype.tick = function(){


	if(this.goingForward){
		if(this.z < 1.5){
			this.z += zScaleSpeed
		}

		if(this.y > 470){
			this.y -= 1
		}

		if(this.speed < this.maxSpeed){
			this.speed += this.velocity

			if (this.speed > this.maxSpeed){
				this.speed = this.maxSpeed
			}
		}
	}

	if (this.goingLeft){
		if (this.spriteRotate < 10){
			this.spriteRotate += 1
		}
		this.x -= this.turnSpeed
	} 

	else if (this.spriteRotate > 5 && this.spriteRotate <= 10 ){
			this.spriteRotate -= 1
	}

	if (this.goingRight){
		if(this.spriteRotate > 0){
			this.spriteRotate -= 1
		}
		this.x += this.turnSpeed
	}
	else if(this.spriteRotate >= 0 && this.spriteRotate < 5 )
	{
		this.spriteRotate += 1
	}

	if (!this.goingForward){
		if(this.z > 0){
			this.z -= zScaleSpeed
		}

		if(this.y < 500){
			this.y += 1
		}

		if(this.speed > 0){
			this.speed -= this.slowDown

			if (this.speed < 0){
				this.speed = 0
			}
		}
	}

		this.resolveCollision()
}

BlueFalcon.prototype.checkCollision = function(ts){
	for(var i = 0; i < ts.orbs.length; i++){
		dx = (this.x - ts.orbs[i].x) * (this.x - ts.orbs[i].x)
		dz = (this.y - ts.orbs[i].z) * (this.y - ts.orbs[i].z)

		dr = (this.collCircleRad + ts.orbs[i].radius) * (this.collCircleRad + ts.orbs[i].radius)

		if(dx + dz <= dr){
			this.colliding = true
			this.crt += 1

			this.collDist = dr
			this.collTS = ts

			if(i == 0){
				this.leftCollide = true			
			}
			else if(i == 1){
				this.rightCollide = true
			}
		}
	}
}

BlueFalcon.prototype.resolveCollision = function(){
	if(this.crt <= this.maxCRT && this.crt > 0){
		this.speed = 0

		this.crt += 1

		if(this.goingLeft && this.leftCollide){
			this.spriteRotate -= 2
			
			this.x += this.crs

			if(this.x < this.collTS.x - this.collTS.spriteWidth/2){
				this.goingLeft = false
			}
		}
		
		if(this.goingRight && this.rightCollide){
			this.spriteRotate += 2
		
			this.x -= this.crs

			if(this.x > this.collTS.x + this.collTS.spriteWidth/2 - 30){
				this.goingRight = false
			}
		}
	}

	if(this.crt > this.maxCRT){
		this.crt = 0
	}

	if(this.colliding){	
		this.colliding = false
		this.leftCollide = false
		this.rightCollide = false
		this.collOrb = null
		this.collDist = 0
	}
}