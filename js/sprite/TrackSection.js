function TrackSection(turnDir, turnIntensity, startX, startY, startZ){
	this.nbOfSegments = 30
	this.turnDir = turnDir //0 = straight, 1 = left, 2 = right
    this.turnIntensity = turnIntensity
	this.segments = []
	this.setupSegments(startX, startY, startZ)
}

TrackSection.prototype.setupSegments = function(startX, startY, startZ){
	for (var i = 0; i < this.nbOfSegments; i++) {
        if(i == 0){
            this.segments.push(new TrackSegment(SEGM_ID,startX,startY,startZ,calcScale(startZ), 1)) //first segment is at starting pos
        }
        else{

            prevSegment = this.segments[i-1]

            if(this.turnDir == 0){
                x = startX
            }else if(this.turnDir == 1){
                x = startX - i * this.turnIntensity
            } else if(this.turnDir == 2){
                x = startX + i * this.turnIntensity
            }

            y = 0 //no hills for now so Y stays 0.

            //need an offset for z...
            //un peu d'algebre, encore (j'ai fait ceci apres le scale)
            //y = ax + b (b au besoin)
            //y = offset, a = multiplicateur, x = previousZ
            //pour z = 575, offset = 3 (testé)
            //3 = a * 575
            //a = 3/575
            //offset = 3/575 * previousZ

            offset = (3/575) * prevSegment.z           

            z = prevSegment.z - prevSegment.spriteHeight + offset
            
            scale = calcScale(z)

            if(i % 2 == 0){
                this.segments.push(new TrackSegment(SEGM_ID,x,y,z,scale, 1))
            }
            else{
                this.segments.push(new TrackSegment(SEGM_ID,x,y,z,scale, 2))
            }
        }
    SEGM_ID += 1
	}
}