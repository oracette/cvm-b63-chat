function Note(element, x, y, player){
	this.element = element
	this.x = x
	this.y = y
	this.element.css("left", this.x)
	this.element.css("top", this.y)
	this.Xspeed = (Math.random()*5 + 2) * player
	this.Yspeed = Math.random()*5 + 2
	this.inWindow = true
}

Note.prototype.tick= function(){
	if(this.y <= 0){
		this.Yspeed = - this.Yspeed
	}
	else if(this.y >= window.innerHeight - 160){
		this.Yspeed = - this.Yspeed
	}

	this.x += this.Xspeed

	this.y -= this.Yspeed

	this.element.css("left", this.x)
	this.element.css("top", this.y)

	if(this.x > window.innerWidth - 100 || this.x <= 0){
		this.inWindow = false
	}
}