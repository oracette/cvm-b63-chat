<?php 
	require_once("action/CommonAction.php");
	require_once('action/lib/nusoap.php');

	class ChatAction extends CommonAction{
		public $theme;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
			$this->theme = $_GET["theme"];
		}
	}