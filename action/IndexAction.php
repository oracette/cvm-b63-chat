<?php
	require_once("action/CommonAction.php");
	require_once('action/lib/nusoap.php');
	
	class IndexAction extends CommonAction {

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
			$this->client = Connection::getConnection();
			$_SESSION["client"] = serialize($this->client);

			if(!empty($_POST['registerSubmit'])){
				if(!empty($_POST['matricule']) && 
					!empty($_POST['nom']) && 
					!empty($_POST['prenom']) && 
					!empty($_POST['pseudo']) && 
					!empty($_POST['mdp']) && 
					!empty($_POST['welcome'])){
					$this->enregistrer($_POST['matricule'], $_POST['prenom'], $_POST['nom'],$_POST['pseudo'],$_POST['mdp'],$_POST['welcome']);
				}
			}
			elseif(isset($_POST['username']) && isset($_POST['password'])){
				$this->login($_POST['username'], $_POST['password']);
			}
		}

		public function login($user,$password){
			$this->key = $this->client->call('connecter', array('nomUsager' => $user, 'motDePasse' => md5($password)));

			if($this->key === "INVALID_USERNAME_PASSWORD"){
				$this->error = "INVALID_USERNAME_PASSWORD";
			}
			elseif($this->key === "USER_IS_BANNED"){
				$this->error = "USER_IS_BANNED";
			}
			else{
				if ($this->client->fault) {
					$this->error = "(" . $this->client->faultcode . ") " . $this->client->faultstring;
				}
				else{
					$_SESSION["key"] = $this->key;
					$cookie_name = "user";
					$cookie_value = $user;
					setcookie($cookie_name, $cookie_value, time() + (86400 * 2), "/"); //2 jours pour le cookie

					$theme =$this->getTheme();
					$url = "location:chat.php?theme=" . $theme;
					header($url);
					exit;
				}
			}
		}

		public function enregistrer($matricule, $prenom, $nom, $pseudo, $mdp, $welcome){
			$this->key = $this->client->call('enregistrer',array('matricule' => $matricule, 'prenom' => $prenom, 'nom' => $nom, 'nomUsager' => $pseudo, 'motDePasse' => md5($mdp), 'texteBienvenue' => $welcome));
			
			if ($this->client->fault) {
					$this->error = "(" . $soapClient->faultcode . ") " . $soapClient->faultstring;
			} 
		}

		public function isLoggedIn() {
			return $_SESSION["visibility"] > CommonAction::$VISIBILITY_PUBLIC;
		}	

		public function getTheme(){
			$theme = "none";

			if(isset($_POST['powTheme']) && isset($_POST['fzeroTheme']) && isset($_POST['saxTheme'])){
				if($_POST['powTheme'] === "true"){
					$theme = "pow";
				}
				elseif($_POST['fzeroTheme'] === "true"){
					$theme = "fzero";
				}
				elseif($_POST['saxTheme'] === "true"){
					$theme = "sax";
				}
			}

			return $theme;
		}
	}