<?php

	require_once('action/lib/nusoap.php');

	class Connection{
		public static $url = 'http://apps-de-cours.com/web-chat/server/services.php';
		public static $wdsl = false;

		public $client;
		public $key;
		public $error;

		public function __construct(){
			$this->connect();
		}

		public function connect(){
			$this->client = new nusoap_client(Connection::$url, Connection::$wdsl);
			$this->error = $this->client->getError();
		}

		public function login($user,$password){
			if (empty($this->error)){
				$this->key = $this->client->call('connecter', array('nomUsager' => $user, 'motDePasse' => md5($password)));

				if ($this->client->fault) {
					$this->error = "(" . $this->client->faultcode . ") " . $this->client->faultstring;
				}
			}
		}

		public function logout(){
			if (empty($this->error)){
				$this->key = $this->client->call('deconnecter', array($this->key));

				if ($client->fault) {
					$this->error = "(" . $this->client->faultcode . ") " . $this->client->faultstring;
				}
			}
		}

		public function getMembres(){
			return $this->client->call('listeDesMembres', array($this->key));
		}

		public function getMessages(){
			return $this->client->call('lireMessages', array($this->key));
		}

		public function envoyerMessage($message){
			$this->client->call('ecrireMessage', array($this->key,$message));
		}
	}