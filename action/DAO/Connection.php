<?php
	require_once('action/lib/nusoap.php');

	class Connection{
		public static $url = 'http://apps-de-cours.com/web-chat/server/services.php';
		public static $wdsl = false;

		public static function getConnection(){
			$client = new nusoap_client(Connection::$url, Connection::$wdsl);
			
			return $client;
		}	
	}