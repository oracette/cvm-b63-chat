<?php
	require_once("action/CommonAction.php");

	class AjaxAction extends CommonAction {
		public $result;
		public $messages;
		public $users;
	
		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
			
		}
	
		protected function executeAction() {
			$this->result = array();

			$this->client = unserialize($_SESSION["client"]);
			$this->key = $_SESSION["key"];

			if(isset($_POST['logout'])){
				$this->logout();
			}

			if(isset($_POST['updateChat'])){
				$this->messages = $this->getMessages();
			}

			if(isset($_POST['updateUsers'])){
				$this->users = $this->getMembres();
			}

			if(isset($_POST['toSend'])){
				$this->envoyerMessage($_POST['toSend']);
			}
		}

		public function logout(){
			$this->key = $this->client->call('deconnecter', array($this->key));

			if ($client->fault) {
				$this->error = "(" . $this->client->faultcode . ") " . $this->client->faultstring;
			}
		}

		public function getMembres(){
			return $this->client->call('listeDesMembres', array($this->key));
		}

		public function getMessages(){
			return $this->client->call('lireMessages', array($this->key));
		}

		public function envoyerMessage($message){
			$this->client->call('ecrireMessage', array($this->key,$message));
		}
	}


	
	
	
	
	
	
