<?php
	require_once("action/IndexAction.php");

	$action = new IndexAction();
	$action->execute();

	require_once("partial/header.php");
?>

<script type="text/javascript" src="js/login.js"></script>

<div class="container" id="loginWrap">
	<div class="row">
		<div class="center-block text-center">
			<h1>CVM Chat</h1>
		</div>
	</div>

<?php  
	if(!empty($action->error)){
?>

	<div class="row">
		<div class="center-block text-center">
			<h3 id="error">ERROR: <?=$action->error?></h3>
		</div>
	</div>

<?php
	}
?>

	<form class="form-horizontal" method="post" action="index.php">
		<div class="row">
		  <div class="form-group">
		    <div class="col-sm-8 col-sm-offset-2">
		      <input class="form-control" id="usr" placeholder="Nom" name="username">
		    </div>
		  </div>
		</div>

		<div class="row">
		  <div class="form-group">
		    <div class="col-sm-8 col-sm-offset-2">
		      <input type="password" class="form-control" id="password" placeholder="Mot de Passe" name="password">
		    </div>
		  </div>
		</div>

		<div class="row">
		  	<div class="form-group">
			    <div class="col-sm-2 col-sm-offset-2">
			      <button class="btn btn-default" name="loginSubmit" type="submit">Login</button>
			    </div>

			    <div class="col-sm-2 col-sm-offset-4" id="createAcc">
				  	<a href="#">Don't have an account?</a>
				</div>
			</div>	
		</div>

		<div class="row row-centered">
			<div class="col-sm-1 col-centered themeBtn" id="pow">
				<img src="images/pow-block-icon.png" class="img-responsive">
				<input type="hidden" id="powInput" name="powTheme" value="false">
			</div>

			<div class="col-sm-1  col-sm-offset-1 col-centered themeBtn" id="fzero">
				<img src="images/blue-falcon-icon.gif" class="img-responsive">
				<input type="hidden" id="fzeroInput" name="fzeroTheme" value="false">
			</div>

			<div class="col-sm-1 col-sm-offset-1 col-centered  themeBtn" id="sax">
				<a href="#" data-toggle="tooltip" title="Put your volume UP!"><img src="images/sax-icon.png" class="img-responsive"></a>
				<input type="hidden" id="saxInput" name="saxTheme" value="false">
			</div>
		</div>
	</form>
</div>

<div class="container" id="createAccWrap">
	<div class="row">
		<div class="col-sm-2 col-sm-offset-4" id="hide">
		  	<a href="#">Login</a>
		</div>
	</div>

	<div class="row">
		<div class="center-block text-center">
			<h1>Register</h1>
		</div>
	</div>

	<!-- $matricule, $prenom, $nom, $nomUsager, $motDePasse, $texteBienvenue -->

	<form class="form-horizontal" method="post" action="index.php">
		<div class="row">
		  <div class="form-group">
		    <div class="col-sm-8 col-sm-offset-2">
		      <input class="form-control" id="matricule" placeholder="Matricule" name="matricule">
		    </div>
		  </div>
		</div>

		<div class="row">
		  <div class="form-group">
		    <div class="col-sm-8 col-sm-offset-2">
		      <input class="form-control" id="prenom" placeholder="Nom" name="nom">
		    </div>
		  </div>
		</div>

		<div class="row">
		  <div class="form-group">
		    <div class="col-sm-8 col-sm-offset-2">
		      <input class="form-control" id="prenom" placeholder="Prenom" name="prenom">
		    </div>
		  </div>
		</div>

		<div class="row">
		  <div class="form-group">
		    <div class="col-sm-8 col-sm-offset-2">
		      <input class="form-control" id="pseudo" placeholder="Pseudo" name="pseudo">
		    </div>
		  </div>
		</div>

		<div class="row">
		  <div class="form-group">
		    <div class="col-sm-8 col-sm-offset-2">
		      <input class="form-control" id="mdp" type="password" placeholder="Password" name="mdp">
		    </div>
		  </div>
		</div>

		<div class="row">
		  <div class="form-group">
		    <div class="col-sm-8 col-sm-offset-2">
		      <input class="form-control" id="welcome" placeholder="Texte de bienvenue" name="welcome">
		    </div>
		  </div>
		</div>

		<div class="row">
		  	<div class="form-group">
			    <div class="col-sm-2 col-sm-offset-2">
			      <button class="btn btn-default" name="registerSubmit" type="submit" value="register">Register</button>
			    </div>
			</div>	
		</div>
	</form>
</div>

<?php 
	require_once("partial/footer.php");

		